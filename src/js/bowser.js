﻿if (bowser.msedge) {
    document.documentElement.classList.add('msedge'); // used by app.enableScroll()
} else if (bowser.msie) {
    document.documentElement.classList.add('msie'); // not currently used for anything
}
if (bowser.mobile) {
    document.documentElement.classList.add('mobile'); // disables fixed footer
} else if (bowser.tablet) {
    document.documentElement.classList.add('tablet'); // does nothing currently
} else {
    document.documentElement.classList.add('desktop'); // enables hover effects
}

if (bowser.android) {
    document.documentElement.classList.add('android'); // used by modal
} else if (bowser.ios) {
    document.documentElement.classList.add('ios'); // used to apply focus
}