import { IExtraInfo } from './extra-info';
import { IImpressions } from './impressions';
import { ILink } from './link';
import { ITag } from './tag';

export interface IBookInfo {
  id: string,
  slug: string,
  heroCover: string
  bookCover: string,
  title: string,
  htmlDescription: string,
  author: ITag[],
  illustrator: ITag[],
  previewImage?: string,
  audiobook: boolean,
  tasks: boolean,
  series?: ITag,
  language?: ITag,
  impressions: IImpressions,
  illustrations?: ITag,
  readingLevel?: ITag[],
  grade?: ITag[],
  category?: ITag[],
  pages?: number,
  resources?: ILink[],
  extraInfo?: IExtraInfo
}
