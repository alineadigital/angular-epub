import { ITag } from './tag';

export interface IImpression {
  percent: number
  tag: ITag
}
