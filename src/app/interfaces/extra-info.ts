export interface IExtraInfo {
  title: string,
  htmlDescription: string
}
