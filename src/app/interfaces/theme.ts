export interface ITheme {
  name: string,
  background: string,
  color: string
}
