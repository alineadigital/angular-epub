export interface IFont {
  name: string,
  value?: string
}
