import { IFont } from './font';
import { IFontSize } from './font-size';
import { ILineHeight } from './line-height';
import { ITheme } from './theme';

export interface ISettings {
  themes: ITheme[],
  fontSizes: IFontSize[],
  lineHeights: ILineHeight[],
  fonts: IFont[],
  columns: number[]
}
