import { IImpression } from './impression';

export interface IImpressions {
  impressions: IImpression[],
  anwsers: number
}
