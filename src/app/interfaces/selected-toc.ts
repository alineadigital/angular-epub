import { IToc } from './toc';

export interface ISelectedToc {
  start: IToc,
  end: IToc
}
