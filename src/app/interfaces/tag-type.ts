export interface ITagType {
  slug: string,
  title: string
}
