import { IFont } from './font';
import { IFontSize } from './font-size';
import { ILineHeight } from './line-height';
import { ITheme } from './theme';

export interface ISetting {
  theme: ITheme,
  fontSize: IFontSize,
  lineHeight: ILineHeight,
  font: IFont,
  column: number
}
