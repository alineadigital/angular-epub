export interface IBook {
  id: string,
  slug: string,
  url: string,
  title?: string,
  lang?: string,
  image?: string
}
