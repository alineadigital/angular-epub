export interface ILineHeight {
  name: string,
  value: number,
  icon: string
}
