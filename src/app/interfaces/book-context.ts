import { ISelectedToc } from './selected-toc';
import { ISetting } from './setting';
import { ISettings } from './settings';
import { IToc } from './toc';

export interface BookContext {
  // book
  url: string,
  layout: string,

  // state
  loading: boolean,
  loadingCount: number,
  percentage: number,
  initial: boolean,
  locationLoaded: boolean,
  bookLoaded: boolean,
  tocLoaded: boolean,
  bookShown: boolean,
  prevShown: boolean,
  nextShown: boolean,
  fullscreen: boolean,
  dropdownOpen: string,
  currentColumn: number,
  selectedToc: ISelectedToc,

  // metadata
  lang: string,
  rtl: boolean,
  toc: IToc[],

  // function
  prev: any,
  next: any,

  // settings
  minSpreadWidth: number,
  setting: ISetting,
  settings: ISettings,

  // epubjs
  book: any,
  rendition: any
}
