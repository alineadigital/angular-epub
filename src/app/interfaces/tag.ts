import { ITagType } from './tag-type';

export interface ITag {
  id: string,
  slug: string,
  title: string;
  type: ITagType
}
