export interface IFontSize {
  index: number,
  headline: string,
  paragraph: string
}
