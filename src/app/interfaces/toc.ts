export interface IToc {
  label: string,
  href: string
}
