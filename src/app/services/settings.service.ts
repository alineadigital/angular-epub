import { Injectable } from '@angular/core';
import { ISetting } from '@portal-app/interfaces/setting';
import { Observable, of } from 'rxjs';
import { ISettings } from '@portal-app/interfaces/settings';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  setting: ISetting;
  settings: ISettings = {
    themes: [
      {
        name: 'light',
        background: 'white',
        color: 'black'
      },
      {
        name: 'dark',
        background: 'black',
        color: 'white'
      },
      {
        name: 'blue',
        background: '#001966',
        color: 'white'
      },
      {
        name: 'purple',
        background: '#ba99fd',
        color: 'black'
      },
      {
        name: 'teal',
        background: '#a7ebe5',
        color: 'black'
      },
      {
        name: 'yellow',
        background: '#fdf5c0',
        color: 'black'
      },
      {
        name: 'pink',
        background: '#fc9fe5',
        color: 'black'
      },
      {
        name: 'orange',
        background: '#fca695',
        color: 'black'
      }
    ],
    fontSizes: [
      {
        index: 1,
        headline: '2em',
        paragraph: '1em'
      },
      {
        index: 2,
        headline: '2.25em',
        paragraph: '1.125em'
      },
      {
        index: 3,
        headline: '2.5em',
        paragraph: '1.25em'
      },
      {
        index: 4,
        headline: '2.75em',
        paragraph: '1.375em'
      },
      {
        index: 5,
        headline: '3em',
        paragraph: '1.5em'
      }
    ],
    lineHeights: [
      {
        name: 'Small',
        value: 1,
        icon: '#icon-line-height-small'
      },
      {
        name: 'Medium',
        value: 1.5,
        icon: '#icon-line-height-medium'
      },
      {
        name: 'Large',
        value: 2,
        icon: '#icon-line-height-large'
      }
    ],
    fonts: [
      {
        name: 'Original'
      },
      {
        name: 'Verdana',
        value: "Verdana, Geneva, sans-serif"
      },
      {
        name: 'OpenDyslexic',
        value: "OpenDyslexic, sans-serif"
      }
    ],
    columns: [1, 2]
  }

  constructor() { }

  getSettings(): Observable<ISettings> {
    return of(this.settings);
  }

  getSetting(): Observable<ISetting> {
    this.setting = JSON.parse(localStorage.getItem('epub'));
    if (!this.setting) {
      this.setting = {
        theme: this.settings.themes[0],
        fontSize: this.settings.fontSizes[3],
        column: this.settings.columns[1],
        lineHeight: this.settings.lineHeights[1],
        font: this.settings.fonts[0]
      }
      localStorage.setItem('epub', JSON.stringify(this.setting));
    }
    return of(this.setting);
  }

  update(setting: ISetting) {
    localStorage.setItem('epub', JSON.stringify(setting));
  }
}
