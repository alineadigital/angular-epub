import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor() { }

  getLocation(url: string): Observable<string> {
    let location = JSON.parse(localStorage.getItem(`epub-${url}`));
    if (!location) {
      location = '';
      localStorage.setItem(`epub-${url}`, JSON.stringify(location));
    }
    return of(location);
  }

  setLocation(url: string, value: string) {
    let location = JSON.parse(localStorage.getItem(`epub-${url}`));
    location = value;
    localStorage.setItem(`epub-${url}`, JSON.stringify(location));
  }
}
