import { Injectable } from '@angular/core';
import { IBookInfo } from '../interfaces/book-info';
import { BookInfoStore } from '../store/book-info.store';

@Injectable({
  providedIn: 'root'
})
export class BookInfoService {

  books: IBookInfo[] = BookInfoStore;

  constructor() { }
}
