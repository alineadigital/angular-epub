import { Injectable } from '@angular/core';
import { IBook } from '@portal-app/interfaces/book';
import { Observable, of } from 'rxjs';
import { BooksStore } from '../store/books.store';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books: IBook[] = BooksStore;

  constructor() { }

  getBooks(): Observable<IBook[]> {
    return of(this.books);
  }

  getBookInfo(id: string): Observable<IBook> {
    return of(this.books.find(book => book.id === id));
  }

  getBook(id: string): Observable<IBook> {
    return of(this.books.find(book => book.id === id));
  }
}
