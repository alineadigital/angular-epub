import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DropdownService {

  open = new BehaviorSubject<string>(null);
  open$ = this.open.asObservable();

  constructor() { }

  openDropdown(dropdown: string) {
    this.open.next(dropdown);
  }

  closeDropdown() {
    this.open.next('');
  }

  toggleDropdown(value) {
    if (value === this.open.getValue() && value !== '') {
      this.open.next('');
    } else {
      this.open.next(value);
    }
  }
}
