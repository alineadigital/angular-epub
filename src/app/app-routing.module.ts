import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('@portal-app/modules/routes/books/books.module').then(module => module.BooksModule),
    data: { pageType: 'books' }
  },
  {
    path: 'book-info/:slug/:id',
    loadChildren: () => import('@portal-app/modules/routes/book-info/book-info.module').then(module => module.BookInfoModule),
    data: { pageType: 'book-info' }
  },
  {
    path: 'book/:slug/:id',
    loadChildren: () => import('@portal-app/modules/routes/book/book.module').then(module => module.BookModule),
    data: { pageType: 'book' }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
    scrollPositionRestoration: 'top',
    enableTracing: false,
    relativeLinkResolution: 'legacy'
}
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
