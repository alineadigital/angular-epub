import { Component, HostListener, OnInit } from '@angular/core';
import { DropdownService } from '@portal-app/services/dropdown.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-epub';

  constructor(private dropdownService: DropdownService) { }

  ngOnInit() { }

  @HostListener('document:click', ['$event.target'])
  private documentClick(target: HTMLElement) {
    if (!target.closest('.dropdown')) {
      this.dropdownService.closeDropdown();
    }
  }
}
