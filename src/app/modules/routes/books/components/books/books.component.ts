import { Component, OnInit } from '@angular/core';
import { BooksService } from '@portal-app/services/books.service';
import { IBook } from '@portal-app/interfaces/book';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  books: IBook[];

  constructor(private booksService: BooksService) { }

  ngOnInit(): void {
    this.getBooks();
  }

  private getBooks() {
    this.booksService.getBooks()
      .subscribe((books) => {
        this.books = books;
      });
  }
}
