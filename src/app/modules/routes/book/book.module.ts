import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from './components/book/book.component';
import { BookRoutingModule } from './book-routing.module';
import { ScriptService } from '@portal-app/services/script.service'
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
  declarations: [BookComponent],
  imports: [
    CommonModule,
    BookRoutingModule,
    MatSliderModule,
    MatSlideToggleModule
  ],
  providers: [
    ScriptService
  ]
})
export class BookModule { }
