import { Component, OnInit, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ScriptService } from '@portal-app/services/script.service';
import { DropdownService } from '@portal-app/services/dropdown.service';
import { ActivatedRoute } from '@angular/router';
import { BooksService } from '@portal-app/services/books.service';
import { MatSliderChange } from '@angular/material/slider';
import { SettingsService } from '@portal-app/services/settings.service';
import { ISetting } from '@portal-app/interfaces/setting';
import { IFont } from '@portal-app/interfaces/font';
import { ITheme } from '@portal-app/interfaces/theme';
import { ILineHeight } from '@portal-app/interfaces/line-height';
import { LocationService } from '@portal-app/services/location.service';
import { BookContext } from '@portal-app/interfaces/book-context';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

declare var ePub: any;
declare var bowser: any;

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  @ViewChild('domElement') domElement: ElementRef;
  @ViewChild('domHeader') domHeader: ElementRef;
  @ViewChild('domFooter') domFooter: ElementRef;
  @ViewChild('domReader') domReader: ElementRef;

  context: BookContext = {
    // book
    url: undefined,
    layout: undefined,

    // state
    loading: false,
    loadingCount: 0,
    percentage: 0,
    initial: true,
    locationLoaded: false,
    bookLoaded: false,
    tocLoaded: false,
    bookShown: false,
    prevShown: false,
    nextShown: false,
    fullscreen: false,
    dropdownOpen: '',
    currentColumn: undefined,
    selectedToc: undefined,

    // metadata
    lang: undefined,
    rtl: undefined,
    toc: [],

    // function
    prev: undefined,
    next: undefined,

    // settings
    minSpreadWidth: 600,
    setting: undefined,
    settings: undefined,

    // epubjs
    book: undefined,
    rendition: undefined
  };

  constructor(
    private route: ActivatedRoute,
    private dropdownService: DropdownService,
    private scriptService: ScriptService,
    private booksService: BooksService,
    private settingsService: SettingsService,
    private locationService: LocationService) { }

  ngOnInit(): void {
    this.dropdownService.open$.subscribe(open => {
      this.context.dropdownOpen = open;
    });

    this.getBook();
  }

  private getBook() {
    this.route.paramMap
      .subscribe(params => {
        const id = params.get('id');

        this.booksService.getBookInfo(id)
          .subscribe((book) => {
            this.context.lang = book.lang;
            this.context.url = book.url;

            this.scriptService.load('jszip', 'epub').then(data => {
              this.initialize();
            }).catch(error => console.log(error));
          });
      });
  }

  onToggleDropdown(value: string) {
    this.dropdownService.toggleDropdown(value);
  }

  initialize() {
    this.context.book = ePub(this.context.url);

    this.context.book.ready.then(() => {
      // this happens only once

      this.locationService.getLocation(this.context.url).subscribe((location) => {
        if (location) {
          this.context.book.rendition.display(location).then(() => {
            this.context.locationLoaded = true;
          }).catch(() => {
            this.context.book.rendition.display().then(() => {
              this.context.locationLoaded = true;
            });
          });
        } else {
          this.context.book.rendition.display()
          this.context.locationLoaded = true;
        }
      });
    });

    this.context.book.loaded.metadata.then((metadata) => {
      this.context.layout = metadata.layout === 'pre-paginated' ? 'pre-paginated' : 'reflowable';
      this.context.rtl = this.context.book.package.metadata.direction === "rtl";

      this.setRendition();

      if (this.context.layout === 'reflowable') {
        this.settingsService.getSetting().subscribe((setting) => {
          this.context.setting = setting;
          this.setSetting(this.context.setting);
          this.setColumn(this.context.setting.column);
          this.resizeReader();
          this.context.bookLoaded = true;
        });
        this.settingsService.getSettings().subscribe((settings) => {
          this.context.settings = settings;
        });
      } else {
        this.context.setting = {
          column: 2,
          font: undefined,
          fontSize: undefined,
          lineHeight: undefined,
          theme: undefined
        }
        this.setColumn(this.context.setting.column);
        this.resizeReader();
        this.context.bookLoaded = true;
      }

      this.context.prev = () => {
        const location = this.context.rendition.currentLocation();
        if (this.context.rtl && !location.atEnd || !this.context.rtl && !location.atStart) {
          this.showLoading();
          return this.context.rtl ? this.context.rendition.next() : this.context.rendition.prev();
        }
      };

      this.context.next = () => {
        const location = this.context.rendition.currentLocation();
        if (this.context.rtl && !location.atStart || !this.context.rtl && !location.atEnd) {
          this.showLoading();
          return this.context.rtl ? this.context.rendition.prev() : this.context.rendition.next();
        }
      };
    });

    this.context.book.loaded.navigation.then((data) => {
      //  // this happens only once

      this.context.book.spine.spineItems.forEach((item) => {
        if (item.linear === false) {
          return;
        }
        const found = data.toc.find(element => element.href.replace('xhtml/', '') === item.href.replace('xhtml/', ''));
        let label = `Side ${item.index + 1}`;
        if (found) {
          label = found.label;
        } else {
          if (item.index === 0) {
            label = 'Cover';
          }
        }
        this.context.toc.push({
          label: label,
          href: item.href.replace('xhtml/', '')
        });
      });

      this.context.tocLoaded = true;
    });
  }

  showLoading() {
    this.context.loadingCount++;
    this.context.loading = true;
  }

  hideLoading() {
    this.context.loadingCount--;
    if (this.context.loadingCount <= 0) {
      this.context.loadingCount = 0;
      this.context.loading = false;
    }
  }

  resizeReader(forceOneColumn?: boolean) {
    const availableWidth = this.domElement.nativeElement.offsetWidth,
      availableHeight = this.domElement.nativeElement.offsetHeight - this.domHeader.nativeElement.offsetHeight - this.domFooter.nativeElement.offsetHeight;

    if (availableHeight > availableWidth) {
      // portrait
      forceOneColumn = true;
    }

    if (forceOneColumn || availableWidth < this.context.minSpreadWidth) {
      this.setColumn(1);
    } else if (availableWidth >= this.context.minSpreadWidth && (this.context.setting.column === 2 || this.context.layout === 'pre-paginated')) {
      this.setColumn(2);
    }

    const contentWidth = this.domReader.nativeElement.offsetWidth,
      contentHeight = this.domReader.nativeElement.offsetHeight,
      scale = Math.min(
        (availableWidth - 20) / contentWidth,
        (availableHeight - 20) / contentHeight
      );

    this.domReader.nativeElement.style.setProperty('width', (contentWidth * scale) + 'px');
    this.domReader.nativeElement.style.setProperty('height', (contentHeight * scale) + 'px');

    if (!forceOneColumn && this.domReader.nativeElement.offsetWidth < this.context.minSpreadWidth && this.context.currentColumn === 2) {
      this.resizeReader(true);
    }

    if (this.context.rendition) {
      this.context.rendition.resize();
    }
  }

  setColumn(column) {
    if (this.context.currentColumn !== column) {
      if (column === 1) {
        this.domReader.nativeElement.style.setProperty('width', (39 * 20) + 'px');
        this.domReader.nativeElement.style.setProperty('height', (50 * 20) + 'px');
        this.context.rendition.spread("none");
      } else if (column === 2) {
        this.domReader.nativeElement.style.setProperty('width', (39 * 20) + 'px');
        this.domReader.nativeElement.style.setProperty('height', (25 * 20) + 'px');
        this.context.rendition.spread(true, this.context.minSpreadWidth);
      }
    }
    this.context.currentColumn = column;
  }

  keyup(e) {
    e.preventDefault();
    e.stopPropagation();
    let target = e.target;
    if (!this.context.dropdownOpen && target.tagName.toLowerCase() !== 'mat-slider') {
      // Left Key
      if ((e.keyCode || e.which) === 37) {
        this.context.prev();
      }

      // Right Key
      if ((e.keyCode || e.which) === 39) {
        this.context.next();
      }
    }
  }

  setSetting(setting: ISetting) {
    this.domElement.nativeElement.style.setProperty('--theme-background', setting.theme.background);
    this.domElement.nativeElement.style.setProperty('--theme-color', setting.theme.color);

    const styleObject = {
      h1: {
        'line-height': '1.2 !important',
        'font-size': setting.fontSize.headline + '!important'
      },
      'p, .toc-short a': {
        'line-height': setting.lineHeight.value + '!important',
        'font-size': setting.fontSize.paragraph + '!important'
      },
      body: {
        'background-color': setting.theme.background + '!important',
        'word-break': 'break-word'
      },
      'body, a, p, h1': {
        color: setting.theme.color + '!important'
      },
      '.block-rw': {
        "border-color": setting.theme.color + '!important'
      }
    };

    if (setting.font.name !== 'Original') {
      styleObject['body, p, h1'] = {
        'font-family': setting.font.value + '!important'
      }
    }

    this.context.rendition.themes.default(styleObject);
  }

  setLocation(location) {
    this.context.nextShown = location.atEnd ? !location.atEnd : true;
    this.context.prevShown = location.atStart ? !location.atStart : true;

    this.locationService.setLocation(this.context.url, location.end.href.replace('xhtml/', ''));

    let index = this.context.toc.findIndex(element => element.href.replace('xhtml/', '') === location.end.href.replace('xhtml/', ''));
    if (index > 0) {
      index++;
    }
    const percentage = Math.round((index / this.context.toc.length) * 100);
    this.context.percentage = percentage;
    this.context.selectedToc = {
      start: {
        label: this.context.toc.find(element => element.href === location.start.href.replace('xhtml/', '')).label,
        href: location.start.href.replace('xhtml/', '')
      },
      end: {
        label: this.context.toc.find(element => element.href === location.end.href.replace('xhtml/', '')).label,
        href: location.end.href.replace('xhtml/', '')
      }
    };
  }

  loadFonts() {
    this.context.rendition.getContents().forEach(c => {
      [
        '/assets/fonts/open-dyslexic.css'
      ].forEach(url => {
        let el = c.document.head.appendChild(c.document.createElement("link"));
        el.setAttribute("rel", "stylesheet");
        el.setAttribute("href", url);
      });
    });
  }

  setRendition() {
    const options: any = {
      width: "100%",
      height: "100%",
      layout: this.context.layout,
      minSpreadWidth: this.context.minSpreadWidth,
      snap: true
    }

    if (bowser.mobile || bowser.tablet) {
      options.manager = 'continuous';
    }

    this.context.rendition = this.context.book.renderTo('epub-reader', options);

    this.context.rendition.on("rendered", (section) => {
      // this happens before relocated on every page shown
      // sometimes only this is called when swiping

      const location = this.context.rendition.currentLocation();
      this.setLocation(location);
    });

    this.context.rendition.on("relocated", (location) => {
      // this happens after rendered on every page shown
      // sometimes this doesn't get called when swiping

      this.setLocation(location);

      this.hideLoading();
      if (this.context.initial) {
        this.context.bookShown = true;
        this.resizeReader();
        this.context.initial = false;
      }
    });

    this.context.rendition.hooks.content.register((contents) => {
      // this happens only once

      this.loadFonts();

      contents.window.addEventListener('click', () => {
        //const target = e.target;

        //if (!target.closest('a') && !target.closest('#audio-players')) {
        //  alinea.speechplayer.prototype.checkClick(next.speechplayer, e, contents.window, this.context.lang);
        //}

        if (this.context.dropdownOpen) {
          this.dropdownService.closeDropdown();
        }
      }, false);

      contents.window.addEventListener('keyup', (e) => {
        this.keyup(e);
      });
    });
  }

  public tocItemClicked(item) {
    this.dropdownService.closeDropdown();
    this.showLoading();
    this.context.rendition.display(item.href.replace('xhtml/', ''));
  }

  public themeItemClicked(theme: ITheme) {
    this.context.setting.theme = theme;
    this.settingsService.update(this.context.setting);
    this.setSetting(this.context.setting);
  }

  public lineHeightItemClicked(lineHeight: ILineHeight) {
    this.context.setting.lineHeight = lineHeight;
    this.settingsService.update(this.context.setting);
    this.setSetting(this.context.setting);
  }

  public fontSizeChange(slider: MatSliderChange) {
    this.context.setting.fontSize = this.context.settings.fontSizes[slider.value - 1];
    this.settingsService.update(this.context.setting);
    this.setSetting(this.context.setting);
  }

  public fontChange(font: IFont) {
    this.context.setting.font = font;
    this.settingsService.update(this.context.setting);
    this.setSetting(this.context.setting);
  }

  public columnChange(toggle: MatSlideToggleChange) {
    this.context.setting.column = toggle.checked ? 2 : 1;
    this.settingsService.update(this.context.setting);
    this.setColumn(this.context.setting.column);
    this.resizeReader();
  }

  progressInput(slider: MatSliderChange) {
    this.domElement.nativeElement.focus();
    const percentage = slider.value;
    this.context.percentage = percentage;
  }

  progressChange(slider: MatSliderChange) {
    this.domElement.nativeElement.focus();
    this.showLoading();
    const percentage = slider.value;
    let index = Math.round((this.context.toc.length / 100) * percentage);

    if (index > 0) {
      index--;
    }

    this.context.rendition.display(this.context.toc[index].href.replace('xhtml/', ''));
  }

  nextClick() {
    this.context.next();
  }

  prevClick() {
    this.context.prev();
  }

  fullscreenClick() {
    this.context.fullscreen = !this.context.fullscreen;
    this.resizeReader();
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(e: KeyboardEvent) {
    this.keyup(e);
  }

  @HostListener('window:resize', ['$event'])
  handleWindowResize() {
    if (this.context.bookShown) {
      this.resizeReader();
    }
  }

  //app.html.on('sidebar-right-change', () => {
  //  if (this.context.bookShown) {
  //    this.resizeReader(context);
  //  }
  //});
}
