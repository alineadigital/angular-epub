import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBook } from '@portal-app/interfaces/book';
import { BooksService } from '@portal-app/services/books.service';

@Component({
  selector: 'app-book-info',
  templateUrl: './book-info.component.html',
  styleUrls: ['./book-info.component.scss']
})
export class BookInfoComponent implements OnInit {

  bookInfo: IBook;

  constructor(private route: ActivatedRoute,
    private booksService: BooksService) { }

  ngOnInit(): void {
    this.getBookInfo();
  }

  getBookInfo() {
    this.route.paramMap
      .subscribe(params => {
        const id = params.get('id');

        this.booksService.getBookInfo(id)
          .subscribe((bookInfo) => {
            this.bookInfo = bookInfo;
          });
      });
  }
}
