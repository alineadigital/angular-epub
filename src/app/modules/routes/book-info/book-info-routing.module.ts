import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookInfoComponent } from './components/book-info/book-info.component';


const routes: Routes = [
  {
    path: '',
    component: BookInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookInfoRoutingModule { }
