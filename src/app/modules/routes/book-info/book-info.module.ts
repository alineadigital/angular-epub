import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookInfoComponent } from './components/book-info/book-info.component';
import { BookInfoRoutingModule } from './book-info-routing.module';



@NgModule({
  declarations: [BookInfoComponent],
  imports: [
    CommonModule,
    BookInfoRoutingModule
  ]
})
export class BookInfoModule { }
