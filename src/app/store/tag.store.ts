import { ITagType } from '@portal-app//interfaces/tag-type';

export const TagTypeStore: ITagType[] = [
  {
    slug: 'author',
    title: ''
  },
  {
    slug: 'illustrator',
    title: 'Illustrator'
  },
  {
    slug: 'series',
    title: 'Series'
  },
  {
    slug: 'language',
    title: 'Language'
  },
  {
    slug: 'impression',
    title: 'Impression'
  },
  {
    slug: 'illustration',
    title: 'Illustration'
  },
  {
    slug: 'reading-level',
    title: 'Reading level'
  },
  {
    slug: 'grade',
    title: 'Grade'
  },
  {
    slug: 'category',
    title: 'Category'
  }
];
