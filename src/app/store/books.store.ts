import { IScript } from '@portal-app/interfaces/script';
import { IBook } from '../interfaces/book';

export const BooksStore: IBook[] = [
  {
    id: '1',
    slug: 'portal-roed-laeseklub-proeve',
    title: 'Portal, Rød Læseklub (Prøve)',
    url: '/assets/books/9788723541826.epub',
    lang: 'da',
    image: '/assets/img/cover/1.jpg'
  },
  {
    id: '2',
    slug: 'ekstremsport-roed-fagklub-proeve',
    url: '/assets/books/9788723544155.epub',
    lang: 'da',
    image: '/assets/img/cover/2.jpg'
  },
  {
    id: '3',
    slug: 'alfahun',
    url: '/assets/books/9788723549457.epub',
    lang: 'da',
    image: '/assets/img/cover/3.jpg'
  },
  {
    id: '4',
    slug: 'stjaeler-du',
    url: '/assets/books/9788723549976.epub',
    lang: 'da',
    image: '/assets/img/cover/4.jpg'
  },
  {
    id: '5',
    slug: 'klubhold-manchester-city-fc-blaa-fagklub',
    url: '/assets/books/9788723550699.epub',
    lang: 'da',
    image: '/assets/img/cover/5.jpg'
  },
  {
    id: '6',
    slug: 'moby-dick',
    url: 'https://s3.amazonaws.com/moby-dick/moby-dick.epub',
    lang: 'en',
    image: '/assets/img/cover/6.jpg'
  },
  {
    id: '7',
    slug: 'amerikansk-fodbold-sort-fagklub',
    url: '/assets/books/Amerikansk_fodbold_Sort_Fagklub.epub',
    lang: 'da',
    image: '/assets/img/cover/7.jpg'
  },
  {
    id: '8',
    slug: 'bmx-blaa-fagklub',
    url: '/assets/books/BMX_Blaa_Fagklub.epub',
    lang: 'da',
    image: '/assets/img/cover/8.jpg'
  },
  {
    id: '9',
    slug: 'uma-kan-spaa',
    url: '/assets/books/Groen_Laeseklub_lydret_Uma_kan_spaa.epub',
    lang: 'da',
    image: '/assets/img/cover/9.jpg'
  },
  {
    id: '10',
    slug: 'koldt-blod-16-tyvebande',
    url: '/assets/books/Koldt_blod_16_-_Tyvebanden.epub',
    lang: 'da',
    image: '/assets/img/cover/10.jpg'
  },
  {
    id: '11',
    slug: 'min-stoerste-hemmelighed-sofie',
    url: '/assets/books/Min_stoerste_hemmelighed_Sofie_Roed_Laeseklub.epub',
    lang: 'da',
    image: '/assets/img/cover/11.jpg'
  },
  {
    id: '12',
    slug: 'palle-laver-fuglehus',
    url: '/assets/books/Palle_laver_fuglehus.epub',
    lang: 'da',
    image: '/assets/img/cover/12.jpg'
  },
  {
    id: '13',
    slug: 'sofie-og-pigebanden',
    url: '/assets/books/Sofie_og_pigebanden.epub',
    lang: 'da',
    image: '/assets/img/cover/13.jpg'
  },
  {
    id: '14',
    slug: 'spejdere-groen-fagklub',
    url: '/assets/books/Spejdere_Groen_Fagklub.epub',
    lang: 'da',
    image: '/assets/img/cover/14.jpg'
  }
];
