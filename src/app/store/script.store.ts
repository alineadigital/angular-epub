import { IScript } from '@portal-app/interfaces/script';

export const ScriptStore: IScript[] = [
  { name: 'jszip', src: '/assets/js/epub/jszip.min.js' },
  { name: 'epub', src: '/assets/js/epub/epub.js' }
];
