import { IScript } from '@portal-app/interfaces/script';
import { IBookInfo } from '../interfaces/book-info';
import { TagTypeStore } from './tag.store';

export const BookInfoStore: IBookInfo[] = [
  {
    id: '1',
    slug: 'portal-roed-laeseklub-proeve',
    heroCover: 'https://edit.org/images/cat/book-covers-big-2019101610.jpg',
    bookCover: 'https://edit.org/images/cat/book-covers-big-2019101610.jpg',
    title: 'Portal, Rød Læseklub (Prøve)',
    htmlDescription: '',
    author: [
      {
        id: '',
        slug: '',
        title: 'Hans Hansen',
        type: TagTypeStore[0]
      },
      {
        id: '',
        slug: '',
        title: 'Jens Jensen',
        type: TagTypeStore[0]
      }
    ],
    illustrator: [
      {
        id: '',
        slug: '',
        title: 'Mikkel Mikkelsen',
        type: TagTypeStore[1]
      },
      {
        id: '',
        slug: '',
        title: 'Peter Petersen',
        type: TagTypeStore[1]
      }
    ],
    previewImage: '',
    audiobook: true,
    tasks: true,
    series: {
      id: '',
      slug: '',
      title: 'Series name',
      type: TagTypeStore[2]
    },
    language: {
      id: '',
      slug: '',
      title: 'Language name',
      type: TagTypeStore[3]
    },
    impressions: {
      impressions: [
        {
          percent: 66,
          tag: {
            id: '',
            slug: '',
            title: 'Nuttede dyr',
            type: TagTypeStore[4]
          }
        },
        {
          percent: 33,
          tag: {
            id: '',
            slug: '',
            title: 'God viden',
            type: TagTypeStore[4]
          }
        }
      ],
      anwsers: 3
    },
    illustrations: {
      id: '',
      slug: '',
      title: 'Mange billeder',
      type: TagTypeStore[5]
    },
    readingLevel: [
      {
        id: '',
        slug: '',
        title: 'Lix 15',
        type: TagTypeStore[6]
      },
      {
        id: '',
        slug: '',
        title: 'Lix 15-20',
        type: TagTypeStore[6]
      }
    ],
    grade: [
      {
        id: '',
        slug: '',
        title: 'Indskoling',
        type: TagTypeStore[7]
      },
      {
        id: '',
        slug: '',
        title: 'Mellemtrin',
        type: TagTypeStore[7]
      }
    ],
    category: [
      {
        id: '',
        slug: '',
        title: 'Dyr',
        type: TagTypeStore[8]
      },
      {
        id: '',
        slug: '',
        title: 'Venskab',
        type: TagTypeStore[8]
      }
    ],
    pages: 27,
    resources: [
      {
        title: 'Opgave',
        url: 'https://fake-url.com/task.doc',
      },
      {
        title: 'Opgaveark',
        url: 'https://fake-url.com/task-sheet.doc',
      }
    ],
    extraInfo: {
      title: 'Titel',
      htmlDescription: '<p>HTML description</p>'
    }
  }
];
