import { Component, OnInit } from '@angular/core';
import { DropdownService } from '@portal-app/services/dropdown.service';

@Component({
  selector: 'app-dropdown-overlay',
  templateUrl: './dropdown-overlay.component.html',
  styleUrls: ['./dropdown-overlay.component.scss']
})
export class DropdownOverlayComponent implements OnInit {

  open: string;

  constructor(private dropdownService: DropdownService) {

    this.dropdownService.open.subscribe((v) => {
      //console.log(v);
      this.open = v;
    });

  }
  ngOnInit() {
  }
}
